/**
 * Created by stephen on 3/16/15.
 */


/////////////////////////////////////////////////////////////////////////////
//
// from https://gist.github.com/michaellopez/10117149
// Scrapes the inbox for new mail every 5 seconds and displays a notification for each new message. Does not support new messages in a conversation. I.e. 1 row in the inbox = 1 notification. Only tested on OS X Mountain Lion. This will break when Gmail changes their DOM!
// 

function notify(title, msg) {
	window.webkitNotifications.createNotification('', title, msg).show();
}

var handledMessages = [];
var count = 0;
function getNewMessages () {
	var messages = [], i, n, span, from;
	var tr = document.querySelectorAll('div.ae4 table tr.zE');

	for(i = 0, n = tr.length; i < n; ++i) {
		span = tr[i].querySelector('span[email]');
		messages.push({
			id: tr[i].id,
			from: span.getAttribute('name') + ' <' + span.getAttribute('email') + '>',
			title: tr[i].querySelector('td[role=link] span b').textContent,
			body: tr[i].querySelector('td[role=link] span.y2').textContent
		});
	}

	return messages;
}

var firstRun = true;
setInterval(checkNewMail, 5000);
function checkNewMail () {
    
    //TODO
    return;
    
	try {
		var i, n, messages = getNewMessages();
		for (i = 0, n = messages.length; i < n; ++i) {
			if (handledMessages.indexOf(messages[i].id) < 0) {
				handledMessages.push(messages[i].id);
				if (!firstRun) {
					notify(messages[i].title, messages[i].from + "\n" + messages[i].body);
				}
			}
		}
		firstRun = false;
	} catch (e) {
		console.log(e);
	}
}







////////////////////////

//window.fluid.addDockMenuItem("mytitle", onclickHandler)
//window.fluid.removeDockMenuItem("mytitle")
// label:--me-- in:inbox

function TODO_smh_showNotification(){
    //see https://gist.github.com/code937/5593897
window.fluid.activate();
window.fluid.showGrowlNotification({
    title: "title", 
    description: "description", 
    priority: 1, 
    sticky: false,
    identifier: "foo",
    onclick: callbackFunc,
    icon: imgEl // or URL string
});
}




/////////////////////////////////////////////////////////////////////////////
//
// https://gist.github.com/rawsyntax/594533
//

// ==UserScript==
// @name        Gmail Growl+Sound+Badge (Fluid/Fluidium SSB)
// @namespace   http://userscripts.org/scripts/show/57672
// @description Gmail Growl Notification with Sound Alert and Dock Badge for Fluid
// @include     http://mail.google.com/*
// @include     http://*.google.com/mail/*
// @include     https://mail.google.com/*
// @include     https://*.google.com/mail/*
// @author      Tom Hensel
// @attribution shellshock (http://userscripts.org/scripts/review/57672), Nicky Leach (http://userscripts.org/scripts/show/56774)
// @copyright   2010+, Tom Hensel (http://tom.interpol8.net/)
// @license     (CC) Attribution Non-Commercial Share Alike; http://creativecommons.org/licenses/by-nc-sa/3.0/
// @version     0.0.3
// @require     http://usocheckup.dune.net/72181.js
// ==/UserScript==
 
(function () {
    
	if (!window.fluid) {
		alert("This script is meant to be run in Fluid! You should disable it.");
		return;
	}
	
	//TODO
	return;
 
	// Global vars
	var unreadMsgCount = 0;
	var firstcheck;
	var intervalcheck;
 
	// Script options
	var initialDelay = 5;		// seconds to wait for the first check
	var pollInterval = 7.5;		// seconds to wait between checks
	var priority = 1;		// Growl preference
	var sticky = false;		// Growl preference
 
 
	function growlNewMessages() {
		var oldCount = unreadMsgCount;
	
		// Locate the DIV containing the Inbox hyperlink
		var frame = document.getElementById("canvas_frame");
		if( frame ) {
			var inboxElement = frame.contentDocument.getElementsByClassName("n0").item(0);
	
			if (inboxElement) {
				
				// Grab the title of the Inbox hyperlink and locate the
				// unread message count
				var inboxLinkTitle = inboxElement.title;
				matches = inboxLinkTitle.match(/\((\d*)\)/);
			
				if (matches) {
					unreadMsgCount = matches[1];
				} else {
					unreadMsgCount = 0;
				}
			}
		}
 
		// If the unread message count is greater than it was the last
		// time we checked, we know that we've received one or more new
		// messages.
		if (unreadMsgCount > oldCount) {
			// Play default system alert sound (see OSX "Sound" Preferences Pane)
			window.fluid.beep();
			// Show Growl notification
			window.fluid.showGrowlNotification({
				title: "Gmail",
				description: unreadMsgCount + " unread message(s)",
				priority: priority,
				sticky: sticky
			});
			// Show Badge notification
			window.fluid.dockBadge = unreadMsgCount;
		}
		
		// If you've read some messages since the last check: Show new Badge
		else if (unreadMsgCount < oldCount) {
			// There are still some unread messages: Show new number
			if (unreadMsgCount > 0) {
				window.fluid.dockBadge = unreadMsgCount;
			}
			// All messages are read since the last check: Clear Badge
			else {
				window.fluid.dockBadge = "";
			}
		}
	}
	
	//Run the 1st check after [initialDelay] seconds
	firstcheck = window.setTimeout(function(){growlNewMessages();}, initialDelay * 1000);
	// Check for new messages every [pollInterval] seconds
	intervalcheck = window.setInterval(function(){growlNewMessages();}, pollInterval * 1000);
	
})();

