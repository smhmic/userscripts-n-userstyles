/**
 * Created by stephen on 3/12/15.
 */


console.info('LOADING pfx-hhq-ql');

'use strict';

//TODO: quicklinksQueue don't fire when they dont change the hash



// Multiline Function String - Nate Ferrero - Public Domain
function HEREDOC( f ){
	return f.toString().match( /\/\*\s*([\s\S]*?)\s*\*\//m )[1];
};


$('head' ).append( $('<style>' ).html( HEREDOC( function(){/*

//TODO
:target {
border: 16px solid green;
background: yellow;

}
tr:target {
border: 16px solid blue;
background: red;
}

.js-new-time-entry .text {
    display:none;
}

.js-save-as-pfx-ql {
  left: 4px;
  position: absolute;
  top: 4px;
  font-size: 11px;
  text-transform: uppercase;
  opacity: 0.5;
}
.js-save-as-pfx-ql:hover {
	opacity:1;
}






#pfx-quicklinks {
	
	position: absolute;
	top: 160px;
	font-size: 0.85em;
	
	margin-left: 20px;
	
	text-align:center;
	margin-left: 5px;

}



.js-new-time-entry #pfx-quicklinks {
    top: 70px;
    white-space: pre-line;
    margin-left: -15px;
    text-shadow: none;
    font-weight:400;
    
    
    text-align: right;
    right: -7px;
}

.pfx-quicklink {
    display:inline-block !important;
    padding: 0.3em 1em 0.4em 0.7em;
    margin: 0 -1em 0 -0.7em;
    color: #777;
    display:block !important;
    margin: 0 auto;
    position:relative;
    opacity:0.9;
}
.pfx-quicklink:hover,
.ql-action:hover {
	cursor: pointer;
	color: #f36c00;
	background: white;
	opacity:1 !important;
    text-decoration:none;
}
#pfx-quicklinks .pfx-quicklink.ql-action-hover  {
    color: #777;
}
.pfx-quicklink:active,
.ql-action:active {
	//top: 1px;
}


// Generic Project link
.pfx-quicklink.-project.-no-task {
	font-size: 1.2em;
	font-weight:900;
	opacity: 0.4;
}

// Task link 
.pfx-quicklink.-task {
}

// has note
.pfx-quicklink.-note {
}

// has time
.pfx-quicklink.-time {
}

// specific actions
.pfx-quicklink.-action {}

//////

// access meta info and actions by holding alt key

.ql-meta {
	position: absolute;
  display: none;
	left: 100%;
	margin: 0 0 0 -6px;
	background: white;
	line-height:1;
	border: silver;
	font-size: 14px;
  box-shadow: 0 1px 5px silver;
  border-radius:99px;
  padding: 0px 4px 2px;
  vertical-align: baseline;
	color: #555;
}
.ql-meta:hover {
	color: #f36c00;
}
.ql-meta:active {
margin-top:1px;
box-shadow: 0 0 3px silver;
}

#pfx-quicklinks.alt-hover .pfx-quicklink:hover {
	//box-shadow: 0 0 5px -1px silver;
}
#pfx-quicklinks.alt-hover .pfx-quicklink:hover .ql-meta,
#pfx-quicklinks           .pfx-quicklink       .ql-meta:hover, 
#pfx-quicklinks           .pfx-quicklink       .ql-meta:active {
  display: inline-block !important;
}

*/} )
  // convert line comments to proper css comments
  //.replace(/^\/\/\s?(.*)$/mg,'/* $1 */')
  // remove line comments
  .replace(/^\s*\/\/(.*)$/mg,'')

));







//////////////////////////////////////////




(function(){

	
	if( window.pfx_hhq_ql ){
		//return console.count( 'Prevented multiple loads of pfx_hhq_ql' );
	}
	
	

var o = {};
//var o = window;
	
window.pfx_hhq_ql = o;
/*
quicklink = {
	html    : '', // shows as link text
	desc    : '', // defaults to: "project > task (note - time)"
	//
	project : {},
	task    : {},
	note    : '',
	time    : 0,
	action  : TIME_START | TIME_FOCUS_PROJECT | TIME_FOCUS_TASK | TIME_FOCUS_NOTE | TIME_FOCUS_TIME
}
 */



var keepTryingFn = function( fn, tryfor, freq ){
	tryfor = tryfor || 300;
	freq = freq || 30;
	var keepTryingFnHelper = function(){
		fn();
		if( tryfor >= 0 ){
			tryfor = tryfor - freq;
			window.setTimeout( keepTryingFnHelper, freq );
		}
	};
	keepTryingFnHelper();
}

var elSustainFocus = function( $target, tryfor, freq ){

	keepTryingFn( function(){
		$target.focus();
		var el = $target[0];
		//el.focus();
		if( typeof el.selectionStart == "number" ){
			el.selectionStart = 0;
			el.selectionEnd = el.value.length;
		} else if( undefined !== el.createTextRange ){
			el.focus();
			var range = el.createTextRange();
			range.collapse( false );
			range.select();
		}
		if( tryfor >= 0 ){
			tryfor = tryfor - freq;
			window.setTimeout( elSustainFocusHelper, freq );
		}

	}, tryfor, freq );
}



///////////////
// Easy display of categories w/ having to know IDs



// data
o.projects = {};
o.tasks = {};
//o.quicklinksQueue = [];

o.error = function( msg ){
	// TODO: don't alert()
	alert( msg );
	o._logError( msg );
}
o._error = function( msg ){
	if( 1 < arguments.length ){
		msg = arguments;
	}
	throw new Error( msg );
}



if( !"onhashchange" in window ){
	return o.error( 'The Harvest Quicklinks plugin will not work in this browser.' );
}

o._handle_hashchange = function( event ){
	
	o._logGroup( 'Handle hashchange', window.location.hash );
	
	var str = window.location.hash, params;
	if( str.indexOf('#/#ql') === 0 ){
		o._log('Hash is a quicklink!');
	}
	else if( str.indexOf( '#ql' ) === 0 ){
		o._log('Harvest changed hash on pageload; restore hash prefix'); // (so that if a user manually copies URLor refreshes page, it won't break Harvest on pageload)
		window.location.hash = '#/'+window.location.hash;
	}
	else {
		// not a quicklink
		o._logGroupEnd('Not a quicklink.');
		return;
	}
	
	if( o._scrollPos ){
		o._log('Preventing scroll to hash target');
		$(document).scrollTop( o._scrollPos);
	}
	
	str = str.replace( /^(#\/)?#ql/, '' );
	try {
		o._log('Parsing hash as JSON.');
		params = JSON.parse( str );
		delete params._;
	} catch( ex ){
		o._logGroupEnd('Invalid quicklink format; should be valid JSON');
		o.error( 'Invalid quicklink format' );
		return;
	}

	o._do_quicklink( params );
	o._logGroupEnd();
	
}
//

o._create_quicklink_hash = function( ql ){
	var hash = 'ql',//'/#ql', 
	 params = {};

	//params._ = '{ ' + ql.desc + ' }';
	
	if( ql.project ){  params.project = ql.project.id; }
	if( ql.task ){     params.task    = ql.task.id; }
	if( ql.note ){     params.note    = ql.note; }
	if( ql.time ){     params.time    = ql.time; }   
	if( ql.action ){   params.action  = ql.action; }

	hash += JSON.stringify( params );
	return hash;
}









o.get_project_info = function( id_or_name ){
	return o._get_dropdown_info( o.entryForm.fields.project(), o.projects, id_or_name );
}
o.get_task_info = function( id_or_name, project_id ){
	return o._get_dropdown_info( o.entryForm.fields.task(), o.tasks, id_or_name, function(){
		o.entryForm._setProject( project_id ); } );
}

o._get_dropdown_info = function( $select, cache, id_or_name, fn ) {

	// TODO: should retain current dropdown value upon return
	
	var found;
	
	if( !id_or_name ){
		return;
	}
	if( cache[id_or_name] ){
		return cache[id_or_name];
	}
	//o.entryForm.cb( function(){
		if( fn ){		
			fn.call();
		}
		switch( typeof id_or_name ){
			
			case 'number':
				o._log( 'Finding option by id: '+id_or_name+'' );
				found = $select.find('option[value='+id_or_name+']');
				break;
			
			case 'string':
				id_or_name = id_or_name.replace( '"', '' );
				o._log( 'Finding option by name: "'+id_or_name+'"' );
				found = $select.find( 'option:contains("' + id_or_name + '")' );
				break;
			
			default:
			   return o.error( "Please pass a string or integer." );
				break;
		}
	//});
	if( found.length !== 1 ){
		o._log( ' - no matches' );
		return; //o.error( "TODO!  upon return, make sure search is performed with string" );
	}
	//var info = {
	cache[ id_or_name ] = {
		name : found.text(),
		id   : parseInt( found.attr( 'value' ) )
	};
	if( id_or_name != cache[ cache[id_or_name].name ] ){
		if( cache[ cache[id_or_name].name ] ){
			// if name conflicts with another name in cache, don't cache them by name
			delete cache[ cache[id_or_name].name ]
		} else {
			cache[ cache[id_or_name].name ] = cache[id_or_name];
		}
	}
	cache[ cache[id_or_name].id ] = cache[id_or_name];
		
	return cache[id_or_name];
}




/*
quicklink = {
	html    : '', // shows as link text
	desc    : '', // defaults to: "project > task (note - time)"
	//
	project : {},
	task    : {},
	note    : '',
	time    : 0,
	action  : TIME_START | TIME_FOCUS_PROJECT | TIME_FOCUS_TASK | TIME_FOCUS_NOTE | TIME_FOCUS_TIME
}
*/

o.add_quicklink = function( html, project, task, note, time ){
	
	o._logGroup( 'Adding quicklink', Array.prototype.slice.call(arguments) );
	
	/*
	if( arguments.length == 1 ){
		if( 'object' == typeof arguments[0] ){
			return o._add_quicklink( arguments[0] );
		}
	}
	else if( arguments.length == 2 ){
		if( 'object' == typeof arguments[1] ){
			arguments[1].text = text;
			return o._add_quicklink( arguments[1] );
		}
	}
	*/
	var opts = {};
	/*
	if( html ){     opts.html    = html; }
	if( project ){  opts.project = project; }
	if( task ){     opts.task    = task; }
	if( note ){     opts.note    = note; }
	if( time ){     opts.time    = time; }
	//if( action ){   opts.action  = action; } 
	*/
	
	
	if( html ){
		opts.html = html;
	} else {
		o._logGroupEnd();
		return o.error( 'Must provide text' );
	}
	
	if( project ){  opts.project = o.get_project_info( project ); }
	
	console.log( opts.project );
	
	if( task ){
		if( ! project ){
			return o.error( 'Cannot select task without selecting a project' );
		}
		//TODO: check opts.project annd opts.project.id
		opts.task = o.get_task_info( task, opts.project.id );
	}
	console.log( opts.task );

	if( note ){     opts.note    = note; }
	if( time ){     opts.time    = time; } // TODO: normalize?
	//if( action ){   opts.action  = action; } // TODO: normalize
	
	o._add_quicklink( opts );
	
	o._logGroupEnd();
}

o._add_quicklinks = function( data ){
	for( var i in data ){
		var ql = data[i];
		o._add_quicklink( ql );
	}
}
o._add_quicklink = function( opts ){
	
	o._logGroup( 'Add quicklink', opts );
	
	try{
	
		var quicklink = {};
		
		if( opts.project ){  quicklink.project = opts.project; }
		if( opts.task ){     quicklink.task    = opts.task; }
		if( opts.note ){     quicklink.note    = opts.note; }
		if( opts.time ){     quicklink.time    = opts.time; }   
		if( opts.action ){   quicklink.action  = opts.action; } 
	
		quicklink.html = opts.html || o._make_desc( quicklink );
		quicklink.desc = opts.desc || o._make_desc( quicklink );
		
		o._log( 'Created quicklink object', quicklink);
	
		//o.quicklinksQueue.push( quicklink );
		
		o.settings.addQuicklink( quicklink );
			
		try{
			o.settings.save();
			
			try{
				if( o.quicklinksRenderedFlag ){
					//TODO: indtead of using "qlRendered" list, use a "qlRenderQueue" queue
					o._render_quicklinks();
					o.entryForm.close();
				}
			}catch(ex){
				o._logError('Failed to render quicklink');
				return o._logGroupEnd();
			}
			
		}catch(ex){
			o._logError('Failed to save quicklink');
			return o._logGroupEnd();
		}
		
	}catch(ex){
		o._logError('Failed to add quicklink');
		return o._logGroupEnd();
	}
	
	// TODO: check if already rendered quicklinksQueue ... if so, then render this one
	
	o._logGroupEnd();
	
}


	o.entryForm = {
		
		$ : undefined,
		
		open : function(){
			o._log( 'Opening entryForm' );
			if( o.entryForm.$ ){
				if( o.entryForm.$.is(':visible') ){
					o._log( 'entryForm is already open.' );
					return; // o._error( 'entryForm is already open' );
				}
				o.entryForm.close();
			}
			$( '.js-new-time-entry' ).click();
			//_injectSaveAsQuicklinkButton

			o.entryForm.$ = $('.new-entry-form');
			if( o.entryForm.$.length !== 1 ){
				o.entryForm.$ = undefined;
				return o._error( 'Could not find $(.new-entry-form)' );
			}
			
			//var $submit = $( '.new-entry-form [type=submit]' );
			//$submit.parent().append( $submit.clone().removeClass('js-save') );
		},
		close : function(){
			o._log( 'Closing entryForm.' );
			//TODO: need to check if is open?
			o.entryForm.$ = undefined;
			o.entryForm._elements = {};
			o.entryForm.buttons.cancel().click();
		},
		cb : function( cb, stayOpen ){
			
			o._log( 'CALLBACK entryForm.' );

			// make sure form is accessible
			o.entryForm.open();

			cb.call(o.entryForm);

			if( !stayOpen ){
				o.entryForm.close();
			}
		},
		
		_injectSaveAsQuicklinkButton : function(){
			//o.entryForm.$
			$('.new-entry-form').siblings('h1').prepend( $('<a class="js-save-as-pfx-ql button button-small button-deemphasize" href="#">Save as Quicklink</a>' ).click(function(e){
				e.preventDefault();
				//TODO
				var html = undefined,//'test text',
				   desc = undefined,//'test desc',
				 action = undefined;
				try {
					o.entryForm.saveAsQuicklink( html, desc, action );
				}catch( ex ){
					o._error( 'FAILED TO SAVE AS QL' );
				}
			}) );
		},
		
		saveAsQuicklink : function( html, desc, action ){
			
			var opts = o.entryForm._getParams();
			
			if( html ){   opts.html =   html; }
			if( desc ){   opts.desc =   desc; }
			if( action ){ opts.action = action; }
			
			o._add_quicklink( opts );
		},
		
		_getParams : function(){
			var params = {};
			var keys = ['project','task','note','time'];
			for( var i in keys ){
				var k = keys[i];
				var val = o.entryForm.fields[ k ]().val();
				if( val ){
					params[k] = val;
				}
			}
			return params;
		},
		
		_getProject : function( projectId ){
			return o.get_project_info( projectId || o.entryForm.fields.project().val() );
		},
		_getTask : function( taskId, projectId ){
			return o.get_task_info( taskId || o.entryForm.fields.task().val(), projectId || o.entryForm.fields.project().val() );
		},
		_getNote : function(){
			return o.entryForm.fields.note().val( value );
		},
		_getTime : function(){
			return o.entryForm.fields.time().val( value );
		},
		
		
		_setProject : function( projectId ){
			o._log( 'Setting project: '+projectId );
			o.entryForm.fields.project().val( projectId )
			 .trigger( "liszt:updated" ) // update chosen/liszt dropdown
			 .change(); // triggers update to tasks dropdown options
		},
		_setTask : function( taskId ){
			o._log( 'Setting task: '+taskId );
			o.entryForm.fields.task().val( taskId ).trigger( "liszt:updated" ).change();
		},
		_setNote : function( value ){
			o._log( 'Setting note: "'+value+'"' );
			o.entryForm.fields.note().val( value );
		},
		_setTime : function( value ){
			o._log( 'Setting duration: '+value );
			o.entryForm.fields.time().val( value );
		},
		
		//
		
		_elements : {},
		_getElement : function( selector ){
			if( ! o.entryForm._elements[selector] ){
				if( ! o.entryForm.$ ){
					o.entryForm.open();
					//return o._error( 'Cannot get field "'+selector+'" because entryForm is not open' );
				}
				var $f = o.entryForm.$.find(selector);
				if( $f.length !== 1 ){
					return o._error( 'Found '+ $f.length+' elements matching "'+selector+'"' );
				}
				o.entryForm._elements[selector] = $f;
			}
			return o.entryForm._elements[selector];
		},
		
		//getProjectField = function(){},
		//getProjectObj = function(){},
		 
		 
		fields : {
			project : function(){ return o.entryForm._getElement('.js-project'); },
			task : function(){ return o.entryForm._getElement('.js-tasks'); },
			note : function(){ return o.entryForm._getElement('#entry-notes'); },
			time : function(){ return o.entryForm._getElement('#entry-duration'); },
			/*
			o.fields.project = {
			$input    : $( 'select.js-project' ),
			$search   : $( 'select.js-project + .chzn-container .chzn-search input' ),
			$dropdown : $( 'select.js-project + .chzn-container .chzn-results' ),
			*/
			
		},
		buttons : {
			submit : function(){ return o.entryForm._getElement('.js-save'); },
			//start : function(){ return o.entryForm._getElement('[type=submit]'); },
			//save : function(){ return o.entryForm._getElement('[type=submit]'); },
			cancel : function(){ return o.entryForm._getElement('.js-cancel'); },
		},
		
	};
	//
	
//o.$win = $(window);

o.init = function(){
	
	o._logGroup( 'Initializing' );
	
	//TODO: figure a better way to do this
	$( '.js-new-time-entry' ).click(function(){
		window.setTimeout(function(){
			if( $('.new-entry-form' ).length ){
				o.entryForm._injectSaveAsQuicklinkButton();
			}
		}, 100)
	});
	

// Create html tag in which quicklinksQueue will be placed
	o.$quicklinks_container = $( '<div id="pfx-quicklinks"></div>' );
	// need to intercept clicks on quicklinksQueue if #pfx-quicklinks will be inside new time entry link (under button).
	// otherwise, click will register as simple 'New Entry' button press, and hash will be ignored.
	o.$quicklinks_container.click(function(e){
		e.stopPropagation();
	});
	o.$quicklinks_container.mouseover(function(e){
		e.stopPropagation();
		o.$quicklinks_container._is_mouseover = true;
		if( e.altKey ){
			o.$quicklinks_container.addClass('alt-hover');
		}
	});
	o.$quicklinks_container.mouseout(function(e){
		o.$quicklinks_container._is_mouseover = false;
		o.$quicklinks_container.removeClass('alt-hover');
	});
	$(document).keydown(function(e){
		if( e.altKey && o.$quicklinks_container._is_mouseover ){
			o.$quicklinks_container.addClass('alt-hover');
		}
	}).keyup(function(e){
		if( ! e.altKey ){
			o.$quicklinks_container.removeClass('alt-hover');
		}
	});

	
	
	//$('#main_content').prepend(o.$quicklinks_container);
	$( '.js-new-time-entry' ).append( o.$quicklinks_container );
	o.preventQuicklinksContainerScreenLeftOverflow();
	$(window).resize( o.preventQuicklinksContainerScreenLeftOverflow );
	
	// handle hash on page request
	o._handle_hashchange();
	// and quicklink clicks while on page
	$( window ).bind( 'hashchange', o._handle_hashchange );
	
	
	o.settings.load();
	//o.quicklinksQueue = o._settings.quicklinks || [];
	o._render_quicklinks();
	
	
	//give rows ids so that the hash will match them as :target
	o._log('Give rows ids')
	$('.day-view-entry-list tr' ).each(function(){
		var $this = $( this ) ;
		var rowEntry = o._extractEntryFromTableRow( $this );
		if( rowEntry.time && ( 0 === rowEntry.time ) ){
			delete rowEntry.time;
		}
		$this.attr('id', '/#'+o._create_quicklink_hash(rowEntry) );
	});
	
	o._logGroupEnd();
}
	
	
	// necessary if #pfx-quicklinks is inside new time entry link (under button).
	o.preventQuicklinksContainerScreenLeftOverflow = function() {
		o.$quicklinks_container.css('marginLeft',
		 //( 0 - ( o.$quicklinks_container.parent().offset().left + parseInt(o.$quicklinks_container.parent().css('padding-left')) ) )
		 ( 0 - o.$quicklinks_container.parent().offset().left  )
		);
	}

o._make_desc = function( ql ){
	var r = '';
	r += ql.project && ql.project.name || '*';
	r += ' > ';
	r += ql.task && ql.task.name || '*';
	if( ql.note || ql.time ){
		r += ' (';
		r += ql.note ? '"'+( ql.note )+'"' : '';
		r += ( ql.note && ql.time ? ' - ' : '' );
		r += ql.time ? ( (ql.time<1?(ql.time*60)+' min':ql.time+' hr'+(ql.time>1?'s':'')) ) : '';
		r += ')';
	}
	//TODO - opts.action
	return r;
}
	
	

	
o.actions = {
	
	// action : TIME_START | TIME_FOCUS_PROJECT | TIME_FOCUS_TASK | TIME_FOCUS_NOTE | TIME_FOCUS_TIME
	
	start : function(){
		var submitText = o.entryForm.buttons.submit().val();
		o._log( 'Submitting.' );
		o.entryForm.buttons.submit().click();
		if( 'Start Timer' == submitText ){
			// timer has started
		} else if( 'Save Entry' == submitText ){
			// entry was merely saved, must manually start
			//o._extractEntryFromTableRow( $('.day-view-entry-list tr:last-child') );
			o._log( 'Starting last row timer.' );
			$('.day-view-entry-list tr:last-child .js-start-timer' ).click();
		}
	},	
	submit : function(){
		o._log( 'Submitting.' );
		o.entryForm.buttons.submit().click();
	},
	
	
	focus : {
		project : function(){
			o._log( 'Focusing project field.' );
			keepTryingFn(function(){
				o.entryForm.fields.project().trigger( 'liszt:activate' );
			});
		},
		task : function(){
			o._log( 'Focusing task field.' );
			keepTryingFn(function(){
				o.entryForm.fields.task().trigger( 'liszt:activate' );
			});
		},
		note : function(){
			o._log( 'Focusing note field.' );
			elSustainFocus( o.entryForm.fields.note() );
		},
		time : function(){
			o._log( 'Focusing duration field.' );
			elSustainFocus( o.entryForm.fields.time() );
		},
	},
	
};
	o._doAction = function( str ){
		var action = o.actions;
		var actionsStrings = str.split(',');
		for( var i in actionsStrings ){
			var actionStr = actionsStrings[i];
			var parts = actionStr.split('.');
			if( parts ){
				var accessedParts = [];
				while( parts.length ){
					var part = parts.shift();
					if( action.hasOwnProperty( part ) ){
						action = action[ part ];
						accessedParts.push( part );
					} else {
						return o._error( 'No such action "'+part+'"'+(accessedParts ? ' under "'+accessedParts.join('.')+'"' : '' ) );
					}
				}
				action.call();
			}
		}
	}
	
	o._extractEntryFromTableRow = function( $row ){
		var entry = {};
		//
		$row.find( '.js-edit-entry' ).click();
		entry.project = {
			id : parseInt( o.entryForm.fields.project().val() ),
			name : $row.find('.project' ).text()
		};
		entry.task = {
			id : parseInt( o.entryForm.fields.task().val() ),
			name : $row.find('.task' ).text()
		};
		//
		entry.note = $row.find('.notes' ).text().trim();
		entry.time = parseInt( $row.find('.entry-time' ).text().trim() );
		//
		entry.client = $row.find('.client' ).text().replace(/\s*\((.*)\)\s*/,'$1');
		// date
		// edit()
		// start()
		entry.$ = $row;
		return entry;
	}
	
	
	
	o._TODOgetRows = function() {
		//return $('.day-view-entry-list tr' );
	}
	o._getRowsMatching = function( params ) {
		var matches = [];
		//var rows = o._getRows();
		
		$('.day-view-entry-list tr' ).each(function(){
			var $this = $( this ) ;
			var rowEntry = o._extractEntryFromTableRow( $this );
			
			//give rows ids so that the hash will match them as :target
			var rowEntryTime = rowEntry.time;
			delete rowEntry.time;
			$this.attr('id', o._create_quicklink_hash(rowEntry) );
			rowEntry.time = rowEntryTime;
			
			for( var p in params ){
				if( params.hasOwnProperty(p) ){
					if( rowEntry.hasOwnProperty(p) && params[p] ){
						if( ( p == 'project' ) || (  p == 'task' ) ){
							if( ( rowEntry[p].id != params[p] ) ){
								return;
							}
						} else if ( rowEntry[p] != params[p] ){
							return;
						}
					}
				}
			}
			matches.push( rowEntry );
		});
		return matches;
	}

o._do_quicklink = function( params ){
		o._logGroup( 'Executing quicklink', params );
	
	
	if( params.project && params.task && params.note ){
		o._log('Checking for matching existing entries');
		var rows = o._getRowsMatching( {
			project : params.project,
			task    : params.task,
			note    : params.note,
			//time : params.time
		} );
		o._log('Found '+rows.length+' matching existing entr'+(rows.length==1?'y':'ies'));
		if( rows.length === 1 ){
			o._log('Editing matching row', rows[0]);
			var $row = rows[0].$;
			$row.attr( 'id', window.location.hash.substr( 1 ) );
			$row.find( '.js-edit-entry' ).click();
			o._logGroupEnd();
			return;
		}
	}

	
	o._logGroup( 'Setting paramaters in new entry' );
	o.entryForm.open();

	if( params.project ){
		o.entryForm._setProject( params.project );
		if( params.task ){
			o.entryForm._setTask( params.task );
		}
	}
	
	if( params.note ){
		o.entryForm._setNote( params.note );
	}
	if( params.time ){
		o.entryForm._setTime( params.time );
	}
	o._logGroupEnd();
	
	o.entryForm.$.find( '.button-large' ).click(function(){
		o._logGroup('On entryForm button click ...');
		if( history.pushState ){
		o._log( 'Remove hash via pushState' );
			history.pushState( "", document.title, window.location.href.substring( 0, window.location.href.indexOf( '#' ) ) );
		} else {
			o._log( 'Remove hash and prevent scroll' );
			var scrollPos = $(document).scrollTop();
			window.location.hash = '';
			$(document).scrollTop(scrollPos);
		}
		o._logGroupEnd();
	});
	
	o._logGroup( 'Doing action' );
	if( params.action ){
		o._log( 'Using action parameter', params.action );
		o._doAction( params.action );
	} else {
		o._log( 'Determining default action' );
		if( !params.project ){
			o.actions.focus.project();
		} else if( !params.task ){
			o.actions.focus.task();
		} else if( !params.note ){
			o.actions.focus.note();
		} else if( !params.time ){
			o.actions.focus.time();
		} else {
			o.actions.focus.note();
		}
	}
	o._logGroupEnd();


	/*
	// TODO - ql.action && default action based on which of proj/task/notes provided
	if( note ){
		$( '#entry-notes' ).val( note );
		elSustainFocus( $( '#entry-duration' ) );
	}
	else {
		elSustainFocus( $( '#entry-notes' ) );
	}
	if( ! task ){
		keepTryingFn( function(){
			$( ".js-tasks" )
			 .focus()
				//.trigger('liszt:activate')
				//.trigger('liszt:open')
				//.trigger('liszt:close');
			 .trigger( 'liszt:open' )
			 .trigger( 'liszt:activate' );
		} );
	}
	*/
	
	
		o._logGroupEnd();
}
	
	o._debug = 1;
	o._log = function( msg ) {
		if( o._debug ){
			console.log.apply( console, Array.prototype.slice.call(arguments) );
		}
	}
	o._logGroup = function( msg ) {
		if( o._debug ){
			console.group.apply( console, Array.prototype.slice.call(arguments) );
		}
	}
	o._logGroupEnd = function( msg ) {
		if( o._debug ){
			if( arguments.length ){
				o._log.apply(this,Array.prototype.slice.call(arguments));
			}
			console.groupEnd();
		}
	}
	o._logError = function( msg ) {
		if( o._debug ){
			console.error.apply( console, Array.prototype.slice.call(arguments) );
		}
	}
	
o.quicklinksRenderedFlag = false;
o.quicklinksRendered = [];
o._render_quicklinks = function(){
	o._logGroup('Rendering quicklinks');
	o.quicklinksRenderedFlag = true;
	for( var hash in o._settings.quicklinks ){
		var $ql, ql = o._settings.quicklinks[hash];
		o._logGroup( 'Rendering quicklink',ql);
		
		if( -1 < o.quicklinksRendered.indexOf( hash ) ){		
			o._logGroupEnd('Already rendered '+hash); 
			continue;
		}
		
		$ql = $( '<a class="pfx-quicklink">' )
		//html += '';
		
		if( ql.project ){
			$ql.addClass( '-project' );
			//$ql.attr( 'data-project', ql.project.name );
			if( ql.task ){
				$ql.addClass( '-task' );
				//$ql.attr( 'data-task', ql.task.name );
			} else {
				$ql.addClass( '-no-task' );
			}
		} else {
			$ql.addClass( '-no-project' );
		}
		
		if( ql.note ){
			$ql.addClass( '-note' );
			//$ql.attr( 'data-note', ql.note );
		} else {
			$ql.addClass( '-no-note' );
		}
		
		if( ql.time ){
			$ql.addClass( '-time' );
			//$ql.attr( 'data-time', ql.time );
		} else {
			$ql.addClass( '-no-time' );
		}
		
		$ql.attr( 'title', ql.desc );
		$ql.html( ql.html );
		$ql.append( $( '<b class="ql-meta ql-action" style="display:none">x</b>' ).click((function(hash){return function(e){
			o._logGroup('Deleting quicklink');
			e.stopPropagation();
			e.preventDefault();
			console.error(hash);
			if( o.settings.deleteQuicklink(hash) ){
				o._log('Removing quicklink node from document');
				$(this).parent().remove();
			}
			o._logGroupEnd();
		}})(hash) ).on('mousedown mouseover hover',function(e){
			e.stopPropagation();
		}).hover(function(e){
			$(this).parent().addClass('ql-action-hover');
		},function(e){
			$(this).parent().removeClass('ql-action-hover');
		}));
		$ql.attr( 'href', '#/#'+hash );//o._create_quicklink_hash( ql ) );
		// prevent scrolling to existing matching time entry on click
		$ql.click(function(){ o._scrollPos = $(document).scrollTop(); });
		o.$quicklinks_container.append( $ql );
		//ql.rendered = true;
		o.quicklinksRendered.push( hash );
		o._log( 'Rendered quicklink', $ql[0].outerHTML );
		o._logGroupEnd(); 
	}
	o._logGroupEnd();
	
}

	
	o._settings = {};
	
	o.settings = {
		
		
		addQuicklink : function( ql ){
			var hash = o._create_quicklink_hash( ql );
			/*
			console.log( o._settings.quicklinks );
			if( ! o._settings.quicklinks || (undefined === o._settings.quicklinks) ){
				o._settings.quicklinks = [];
				console.log( o._settings.quicklinks );
			}
			o._settings.quicklinks.push( ql ); // TypeError: Cannot read property 'push' of undefined ... WTF!?
			*/
			if( ! o._settings.quicklinks ){
				o._settings.quicklinks = {};
			}else if( o._settings.quicklinks[hash] ) {
				o.error( 'Quicklink already exists' );
				return;
			}else {
				o._settings.quicklinks[hash] = ql;
			}
		},
		deleteQuicklink : function( hash ){
			o._logGroup('Deleting quicklink: '+hash);
			var r = false;
			if( undefined === o._settings.quicklinks[hash] ){
				o._log('No quicklink with hash: '+hash);
			}else{
				o._log(o._settings.quicklinks[hash]);
				delete o._settings.quicklinks[hash];
				if( undefined === o._settings.quicklinks[hash] ){
					//TODO: bad, bad, bad  v  v  v
					//o._settings.quicklinks.length = o._settings.quicklinks.length-1;
					o._log('Deleted successfully');
					r = true;
				}else{
					o._log('Failed to delete');
				}
			}
			o.settings.save();
			o._logGroupEnd();
			return r;
		},
	
		//storage : {
		
		
		load : function(){
				o._logGroup('Loading');
			var loaded = localStorage.getItem( 'pfx_hhq_ql' ) ;
			if( loaded ){
				o._settings = JSON.parse( loaded );
			} else {
				o._settings = {};
			}
				o._logGroupEnd();
			return;
			
			//TODO: using array ids to delete items, then saving, reloading, can make .ql-action[delete] indexes out of whack, since those indecies are enclosured in the fn which was set upon rendering the quicklinks
			
			//TODO: plus delete does not update array length, which causes problems if try to loop anywhere else
			
			// TODO: handle empty 
			// manual fix: 
			/*
			var indexToDelete = 7;
			console.log(JSON.parse(localStorage.getItem( 'pfx_hhq_ql' )));
			var temp = JSON.parse(localStorage.getItem( 'pfx_hhq_ql' ));
			console.log(temp.quicklinks);
			//delete temp.quicklinks[7];
			//temp.quicklinks.length = temp.quicklinks.length-1;
			//console.log(temp.quicklinks);
			var removedObject = temp.quicklinks.splice(indexToDelete,1);
			console.log(temp.quicklinks);
			//localStorage.clear('pfx_hhq_ql' );
			localStorage.setItem( 'pfx_hhq_ql', JSON.stringify(temp));
			console.log(JSON.parse(localStorage.getItem( 'pfx_hhq_ql' )));
			*/
		},
		
		
	
	
		save : function( ){
				o._logGroup('Saving');
			if( 0 && chrome && chrome.storage && chrome.storage.sync ){
				o._log( 'TODO: via Chrome extension sync storage (chrome.storage.sync)' );
				//TODO
				//https://developer.chrome.com/extensions/storage#property-sync
			}
			else if ( localStorage ){
				o._log( 'via localStorage' );
				if( 0 && localStorage.getItem( 'pfx_hhq_ql' ) ){
					o._log( 'TODO merge settings / check versions' );
					//TODO merge settings / check versions
				} else {
					localStorage.setItem( 'pfx_hhq_ql', JSON.stringify( o._settings ) );
				}
			}
				o._logGroupEnd();

		},
		
		
		clear : function( ){
			o._error('Are you sure??');
			o._settings = {};
			localStorage.clear('pfx_hhq_ql');
		},
		clearQuicklinks : function( ){
			o._error('TODO');
		},
			
		//}
		
		
		
	}

/* 
[EXAMPLES]
  add_quicklink( '<b>Internal</b>', 3340257 );
  add_quicklink( 'Seer Website', '3340257', '1959568' );
  add_quicklink( 'Seer Website bugfix', '3340257', '1959568', 'bugfix' );
    
// PROJECTS //////////////////////////////
//
// 3340257 - Seer > Internal Tasks
//
// TASKS /////////////////////////////////
//
// 1959568 - Seer Website
// 94406   - Housekeeping
// 1959596 - Internal Meeting
// 
*/
	

o.init();
	
	
//TODO: still need to get something like this to work: 
// o.add_quicklink( 'Housekeeping', 'Internal Tasks', 'Housekeeping' );

/*
o.add_quicklink( 'Internal', 3340257 );
o.add_quicklink( 'Housekeeping', 3340257, 94406 );
o.add_quicklink( 'Email', 3340257, 94406, 'email / calendar / misc' );
o.add_quicklink( 'Seer Website Hour', 3340257, 1959568, 'maintainence', 1 );
// the functions above implcitly opened the entryForm to access dropdowns; close it
o.entryForm.close();
o._render_quicklinks();
 */
	

})();

/*
localStorage.clear('pfx_hhq_ql');
*/
/*
pfx_hhq_ql.add_quicklink( 'Internal', 3340257 );
pfx_hhq_ql.add_quicklink( 'Housekeeping', 3340257, 94406 );
pfx_hhq_ql.add_quicklink( 'Email', 3340257, 94406, 'email / calendar / misc' );
pfx_hhq_ql.add_quicklink( 'Seer Website', 3340257, 1959568 );
 */






