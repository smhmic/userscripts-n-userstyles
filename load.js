/**
 * Created by stephen on 3/19/15.
 */

(function(){
	
	
	
	
	(function( domain, path ){
	
		
		
		var folders = {
			
			'com.google' : {
				
				'paths' : {
					'/' : {
						'scripts' : {
							'scriptNameNoExt' : undefined,
						},
						'styles'  : {},
					},
					'/mail/' : {
						'scripts' : {
							'notifications' : undefined,
						},
						'styles'  : {},
					},
				},
				
				
				
				
			}
			
			
		};
		
		var includeScript = function( src ){
	       var script = document.createElement("script");
	       script.src = src;
	       script.async = true;
	       document.body.appendChild(script);
		}
		var includeStyle = function( src ){
	       var stylesheet = document.createElement("link");
	       stylesheet.href = src;
	       stylesheet.rel = 'stylesheet';
	       stylesheet.type = 'text/css';
	       document.head.appendChild(stylesheet);
		}
		
		var scripts = {}, styles = {};
		
		// TODO: match namespace iterative, (like path)
		var domainNamespace = domain.split('.' ).reverse().join('.');
		
		if( domainNamespace in folders ){
			
			//var uriMatches = {};
			var pathParts = path.split( '/' );
			
			var pathToMatch = '/';
			do{
				var uri_match = folders[domainNamespace][pathToMatch];
				if( uri_match ){
					// found matching domain + path
					for( var script_name in uri_match['scripts'] ){
						scripts[ domainNamespace + pathToMatch + script_name + '.user.js' ] = uri_match['scripts'][script_name];
					}
					for( var style_name in uri_match['styles'] ){
						styles[ domainNamespace + pathToMatch + style_name + '.user.css' ] = uri_match['styles'][style_name];
					}
				}
				
				pathToMatch += ''+pathParts.shift()+'/';
			}while( pathParts );
		}
		
		
		
		var includeUrlBase = 'https://bitbucket.org/smhmic/userscripts-n-userstyles';
		
		for( var script_name in scripts ){
			includeScript( includeUrlBase + script_name );
		}
		for( var style_name in styles ){
			includeStyle( includeUrlBase + style_name );
		}
		
		
	})( window.location.hostname, window.location.pathname );
	
})();